package com.mlm.windy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mlm.windy.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByUsername(String username);

    User findUserByEmail(String email);
}
