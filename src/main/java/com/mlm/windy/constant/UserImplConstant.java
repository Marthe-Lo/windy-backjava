package com.mlm.windy.constant;

public class UserImplConstant {
    public static final String USERNAME_ALREADY_EXISTS = "Cet identifiant existe déjà";
    public static final String EMAIL_ALREADY_EXISTS = "Cette adresse-email existe déjà";
    public static final String NO_USER_FOUND_BY_USERNAME = "Aucun utilisateur n'a été trouvé pour cet identifiant: ";
    public static final String FOUND_USER_BY_USERNAME = "Retour de l'utilisateur trouvé par nom d'utilisateur: ";
    public static final String NO_USER_FOUND_BY_EMAIL = "Aucun utilisateur trouvé pour l'e-mail: ";
}
