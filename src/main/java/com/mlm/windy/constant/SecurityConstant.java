package com.mlm.windy.constant;

public class SecurityConstant {
    public static final long EXPIRATION_TIME = 432_000_000; // 5 days expressed in milliseconds
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String TOKEN_CANNOT_BE_VERIFIED = "Le token ne peut être vérifié";
    public static final String SA_WINDY = "Sa windy";
    public static final String WINDY_ADMINISTRATION = "Gestion des utilisateurs";
    public static final String AUTHORITIES = "autorisations";
    public static final String FORBIDDEN_MESSAGE = "Vous devez vous connecter pour accéder à cette page";
    public static final String ACCESS_DENIED_MESSAGE = "Votre profil ne dispose pas de droits suffisants pour accéder à cette page";
    public static final String OPTIONS_HTTP_METHOD = "OPTIONS";
    public static final String[] PUBLIC_URLS = { "/user/login", "/user/register", "/user/image/**" };
    //public static final String[] PUBLIC_URLS = { "**" };
}
