package com.mlm.windy.enumeration;

import static com.mlm.windy.constant.Authority.ADMIN_AUTHORITIES;
import static com.mlm.windy.constant.Authority.EMPLOYEE_AUTHORITIES;
import static com.mlm.windy.constant.Authority.HR_AUTHORITIES;
import static com.mlm.windy.constant.Authority.SUPER_ADMIN_AUTHORITIES;
import static com.mlm.windy.constant.Authority.USER_AUTHORITIES;


public enum Role {
    ROLE_USER(USER_AUTHORITIES),
    ROLE_HR(HR_AUTHORITIES),
    ROLE_EMPLOYEE(EMPLOYEE_AUTHORITIES),
    ROLE_ADMIN(ADMIN_AUTHORITIES),
    ROLE_SUPER_ADMIN(SUPER_ADMIN_AUTHORITIES);

    private String[] authorities;

    Role(String... authorities) {
        this.authorities = authorities;
    }

    public String[] getAuthorities() {
        return authorities;
    }
}
