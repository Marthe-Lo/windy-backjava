package com.mlm.windy.service;

import static com.mlm.windy.constant.EmailConstant.CC_EMAIL;
import static com.mlm.windy.constant.EmailConstant.DEFAULT_PORT;
import static com.mlm.windy.constant.EmailConstant.EMAIL_SUBJECT;
import static com.mlm.windy.constant.EmailConstant.FROM_EMAIL;
import static com.mlm.windy.constant.EmailConstant.GMAIL_SMTP_SERVER;
import static com.mlm.windy.constant.EmailConstant.PASSWORD;
import static com.mlm.windy.constant.EmailConstant.SIMPLE_MAIL_TRANSFER_PROTOCOL;
import static com.mlm.windy.constant.EmailConstant.SMTP_AUTH;
import static com.mlm.windy.constant.EmailConstant.SMTP_HOST;
import static com.mlm.windy.constant.EmailConstant.SMTP_PORT;
import static com.mlm.windy.constant.EmailConstant.SMTP_STARTTLS_ENABLE;
import static com.mlm.windy.constant.EmailConstant.SMTP_STARTTLS_REQUIRED;
import static com.mlm.windy.constant.EmailConstant.USERNAME;
import static javax.mail.Message.RecipientType.CC;
import static javax.mail.Message.RecipientType.TO;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.sun.mail.smtp.SMTPTransport;

@Service
public class EmailService {

	@Async
    public void sendNewPasswordEmail(String firstName, String password, String email) throws MessagingException {
        Message message = createEmail(firstName, password, email);
        SMTPTransport smtpTransport = (SMTPTransport) getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL);
        smtpTransport.connect(GMAIL_SMTP_SERVER, USERNAME, PASSWORD);
        smtpTransport.sendMessage(message, message.getAllRecipients());
        smtpTransport.close();
    }

    private Message createEmail(String firstName, String password, String email) throws MessagingException {
        Message message = new MimeMessage(getEmailSession());
        message.setFrom(new InternetAddress(FROM_EMAIL));
        message.setRecipients(TO, InternetAddress.parse(email, false));
        message.setRecipients(CC, InternetAddress.parse(CC_EMAIL, false));
        message.setSubject(EMAIL_SUBJECT);
        message.setText("Bonjour " + firstName + ", \n \n Votre nouveau mot de passe est le suivant: " + password + "\n \n A votre service, Windy SA");
        message.setSentDate(new Date());
        message.saveChanges();
        return message;
    }

    private Session getEmailSession() {
        Properties properties = System.getProperties();
        properties.put(SMTP_HOST, GMAIL_SMTP_SERVER);
        properties.put(SMTP_AUTH, true);
        properties.put(SMTP_PORT, DEFAULT_PORT);
        properties.put(SMTP_STARTTLS_ENABLE, true);
        properties.put(SMTP_STARTTLS_REQUIRED, true);
        return Session.getInstance(properties, null);
    }
}
